import React from 'react';
import {Card, Layout, Page, Stack, Button, TextStyle} from '@shopify/polaris';

/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Home() {
  return (
    <Page title="Home">
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <Stack alignment="center">
              <Stack.Item fill>
                <p>
                  App status is{' '}
                  <TextStyle variation="strong">disabled</TextStyle>
                </p>
              </Stack.Item>
              <Stack.Item>
                <Button primary>Enable</Button>
              </Stack.Item>
            </Stack>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
