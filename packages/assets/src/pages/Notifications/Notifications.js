import React from 'react';
import {useState} from 'react';
import {
  Card,
  Layout,
  Page,
  ResourceList,
  ResourceItem,
  Stack
} from '@shopify/polaris';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import useFetchApi from '../../hooks/api/useFetchApi';

/**
 * Just render a sample page
 *
 * @param {Function} paginateSamples
 * @param {Object} sample
 * @return {React.ReactElement}
 * @constructor
 */
function Notifications() {
  /**
   * Read the useFetchApi hook, always make API within admin using the api functions from helpers.js
   */

  const {loading, data} = useFetchApi('/notifications');

  const [sortValue, setSortValue] = useState('DATE_MODIFIED_DESC');

  const [selectedItems, setSelectedItems] = useState([]);

  const promotedBulkActions = [
    {
      content: 'Edit',
      onAction: () => console.log('Implement bulk edit')
    }
  ];

  return (
    <Page title="Notifications" subtitle="List of sales notification from">
      <Layout>
        <Layout.Section>
          <Card>
            <ResourceList
              loading={loading}
              resourceName={{singular: 'notification', plural: 'notifications'}}
              items={data}
              selectedItems={selectedItems}
              onSelectionChange={setSelectedItems}
              promotedBulkActions={promotedBulkActions}
              sortValue={sortValue}
              sortOptions={[
                {label: 'Newest update', value: 'DATE_MODIFIED_DESC'},
                {label: 'Oldest update', value: 'DATE_MODIFIED_ASC'}
              ]}
              onSortChange={selected => {
                setSortValue(selected);
              }}
              renderItem={notification => {
                const {
                  id,
                  firstName,
                  city,
                  country,
                  productName,
                  timestamp,
                  productImage
                } = notification;

                return (
                  <ResourceItem id={id}>
                    <Stack>
                      <Stack.Item fill>
                        <NotificationPopup
                          firstName={firstName}
                          city={city}
                          country={country}
                          productName={productName}
                          timestamp={timestamp}
                          productImage={productImage}
                        />
                      </Stack.Item>
                      <Stack.Item>
                        <h3 style={{textAlign: 'right'}}>
                          From Match 8, <br></br>2021
                        </h3>
                      </Stack.Item>
                    </Stack>
                  </ResourceItem>
                );
              }}
            />
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}

export default Notifications;
