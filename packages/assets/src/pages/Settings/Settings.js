import React, {useState, useCallback} from 'react';
import {
  Card,
  Layout,
  Page,
  Tabs,
  Button,
  SkeletonBodyText
} from '@shopify/polaris';
import NotificationPopup from '../../components/NotificationPopup/NotificationPopup';
import Display from '../../components/Display/Display';
import Triggers from '../../components/Triggers/Triggers';
import useFetchApi from '../../hooks/api/useFetchApi';
import defaultSettings from '../../consts/settings';

/**
 * Render a home page for overview
 *
 * @return {React.ReactElement}
 * @constructor
 */
export default function Settings() {
  const {loading, data, update, saveLoading, handleDataChange} = useFetchApi(
    '/settings',
    defaultSettings
  );

  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    selectedTabIndex => setSelected(selectedTabIndex),
    []
  );

  const tabs = [
    {
      id: 'display',
      content: 'Display',
      panelID: 'display',
      body: <Display data={data} handleDataChange={handleDataChange} />
    },
    {
      id: 'triggers',
      content: 'Triggers',
      panelID: 'Triggers',
      body: <Triggers data={data} handleDataChange={handleDataChange} />
    }
  ];

  const skeletonLoading = (
    <>
      <Card.Section>
        <SkeletonBodyText lines={12} />
      </Card.Section>
      <Card.Section>
        <SkeletonBodyText lines={12} />
      </Card.Section>
    </>
  );

  return (
    <Page
      title="Settings"
      subtitle="Decide how your notifications will display"
      primaryAction={
        <Button
          onClick={() => update('/settings', data)}
          primary
          loading={saveLoading}
        >
          Save
        </Button>
      }
      fullWidth
    >
      <Layout>
        <Layout.Section oneThird>
          <NotificationPopup />
        </Layout.Section>
        <Layout.Section>
          <Card title="Tags">
            <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
              {loading ? skeletonLoading : tabs[selected].body}
            </Tabs>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
}
