import Loadable from 'react-loadable';
import Loading from '../../components/Loading';

// eslint-disable-next-line new-cap
export default Loadable({
  loader: () => import('../../pages/NotFound'),
  loading: Loading
});
