export const triggerOptions = [
  {label: 'All pages', value: 'all'},
  {label: 'Specific pages', value: 'specific'}
];

export const ALL_PAGE = 'all';
export const SPECIFIC = 'specific';
