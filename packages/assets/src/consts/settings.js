const defaultSettings = {
  allowShow: 'all',
  displayDuration: 3,
  excludedUrls: '',
  firstDelay: 0,
  hideTimeAgo: false,
  includedUrls: '',
  maxPopsDisplay: 20,
  popsInterval: 2,
  position: 'bottom-left',
  truncateProductName: true
};

export default defaultSettings;
