import React from 'react';
import {Card, Checkbox, FormLayout} from '@shopify/polaris';
import Slider from '../../components/Slider/Slider';
import DesktopPositionInput from '../../components/DesktopPositionInput/DesktopPositionInput';
import PropTypes from 'prop-types';

const Display = ({data, handleDataChange}) => {
  return (
    <>
      <Card.Section title="Appearance">
        <FormLayout>
          <DesktopPositionInput
            label="Desktop Position"
            helpText="The display position of the pop on your website"
            value={data.position}
            onChange={value => handleDataChange('position', value)}
          />
          <Checkbox
            label="Hide time ago"
            checked={data.hideTimeAgo}
            onChange={value => handleDataChange('hideTimeAgo', value)}
          />

          <Checkbox
            label="Truncate content text"
            checked={data.truncateProductName}
            onChange={value => handleDataChange('truncateProductName', value)}
            helpText="If the product name is long for one line, it will be truncated to 'Product na...'"
          />
        </FormLayout>
      </Card.Section>

      <Card.Section title="TIMING">
        <FormLayout>
          <FormLayout.Group>
            <Slider
              value={data.displayDuration}
              name="displayDuration"
              data={data}
              handleDataChange={handleDataChange}
              label="Display duration"
              helpText="How long each pop will display on your page"
            />
            <Slider
              value={data.firstDelay}
              name="firstDelay"
              data={data}
              handleDataChange={handleDataChange}
              label="Time before the first pop"
              helpText="The delay time before the first notification"
            />
          </FormLayout.Group>

          <FormLayout.Group>
            <Slider
              value={data.popsInterval}
              name="popsInterval"
              handleDataChange={handleDataChange}
              data={data}
              label="Gap time between two pops"
              helpText="The time interval between two popup notifications"
            />

            <Slider
              value={data.maxPopsDisplay}
              name="maxPopsDisplay"
              handleDataChange={handleDataChange}
              data={data}
              label="Maximum of popups"
              helpText={`The maximum number of popups are allowed to show
                     after page loading. Maximum number is 80`}
              max="80"
              suffix="pop(s)"
            />
          </FormLayout.Group>
        </FormLayout>
      </Card.Section>
    </>
  );
};

Display.propTypes = {
  data: PropTypes.object,
  handleDataChange: PropTypes.func
};
export default Display;
