import {RangeSlider, TextField} from '@shopify/polaris';
import React from 'react';
import PropTypes from 'prop-types';

const Slider = ({
  value,
  handleDataChange,
  label,
  name,
  helpText,
  max = 100,
  suffix = 'second(s)'
}) => {
  return (
    <RangeSlider
      output
      label={label}
      value={value}
      max={max}
      onChange={value => handleDataChange(name, value)}
      helpText={helpText}
      suffix={
        <div style={{width: '120px'}}>
          <TextField
            value={`${value}`}
            suffix={suffix}
            max={max}
            onChange={value => handleDataChange(name, value)}
          />
        </div>
      }
    />
  );
};

Slider.propTypes = {
  value: PropTypes.number,
  handleDataChange: PropTypes.func,
  label: PropTypes.string,
  name: PropTypes.string,
  helpText: PropTypes.string,
  max: PropTypes.string,
  suffix: PropTypes.string
};
export default Slider;
