import React from 'react';
import {FormLayout, Select, TextField, Card} from '@shopify/polaris';
import {triggerOptions, ALL_PAGE} from '../../consts/settings/triggers';
import PropTypes from 'prop-types';

function Triggers({data, handleDataChange}) {
  return (
    <Card.Section>
      <FormLayout>
        <Select
          label="Pages restriction"
          options={triggerOptions}
          onChange={value => handleDataChange('allowShow', value)}
          value={data.allowShow}
        />

        {data.allowShow !== ALL_PAGE && (
          <TextField
            label="Included pages"
            value={data.includedUrls}
            onChange={value => handleDataChange('includedUrls', value)}
            multiline={4}
            autoComplete="off"
            helpText="Page URLs to show the pop-up (separated by new lines)"
          />
        )}

        <TextField
          label="Excluded pages"
          value={data.excludedUrls}
          onChange={value => handleDataChange('excludedUrls', value)}
          multiline={4}
          autoComplete="off"
          helpText="Page URLs NOT to show the pop-up (separated by new lines)"
        />
      </FormLayout>
    </Card.Section>
  );
}

Triggers.propTypes = {
  data: PropTypes.object,
  handleDataChange: PropTypes.func
};

export default Triggers;
