import webhook from '../consts/webhook';

export async function createWebhook(shopify) {
  const webhooks = await shopify.webhook.list();

  const foundWebhook = webhooks.find(one => {
    return one.address === webhook.address && one.topic === webhook.topic;
  });

  if (foundWebhook) return;
  else {
    await shopify.webhook.create(webhook);
  }
}
