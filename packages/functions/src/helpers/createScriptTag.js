import scriptTag from '../consts/scriptTag';

export async function createScriptTag(shopify) {
  await shopify.scriptTag.create(scriptTag);
}
