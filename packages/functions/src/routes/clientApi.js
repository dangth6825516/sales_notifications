import Router from 'koa-router';
import * as clientController from '../controllers/clientApi/clientController';

const router = new Router({
  prefix: '/clientApi'
});

router.get('/notifications', clientController.get);

export default router;
