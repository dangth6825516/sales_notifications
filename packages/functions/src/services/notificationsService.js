import {createNotifications} from '../repositories/notificationsRepository';

export async function syncOrders(shopify, shopId, shopifyDomain, orderData) {
  if (!orderData) orderData = await shopify.order.list({limit: 30});

  const ids = [
    ...new Set(orderData.map(order => order.line_items[0].product_id))
  ];

  const products = await shopify.product.list({ids: ids.join(',')});

  const getProductImage = productId => {
    const product = products.find(product => product.id === productId);
    return product?.images?.[0] || null;
  };

  const postData = orderData.map(order => {
    const address = order.shipping_address;
    const lineItem = order.line_items[0];
    const productImage = getProductImage(lineItem.product_id);

    return {
      firstName: address.first_name,
      city: address.city,
      country: address.country,
      productName: lineItem.name,
      productId: lineItem.product_id,
      timestamp: new Date(order.created_at),
      productImage,
      shopId,
      shopifyDomain
    };
  });

  return await createNotifications(postData);
}
