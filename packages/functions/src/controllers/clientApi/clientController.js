import {getNotifications} from '../../repositories/notificationsRepository';
import {getSettings} from '../../repositories/settingsRepository';
import {getShopByShopifyDomain} from '@avada/shopify-auth';

/**
 *
 * @param {Object} ctx
 * @returns {Promise<Object>}
 * @throws {Error}
 */
export async function get(ctx) {
  try {
    const {shopifyDomain} = ctx.request.query;
    const {id} = await getShopByShopifyDomain(shopifyDomain);
    const [notifications, settings] = await Promise.all([
      getNotifications(id),
      getSettings(id)
    ]);

    const data = {settings, notifications};

    return (ctx.body = {
      data,
      success: true
    });
  } catch (e) {
    ctx.status = 404;
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}
