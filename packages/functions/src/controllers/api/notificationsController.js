import {getNotifications} from '../../repositories/notificationsRepository';
import {getCurrentUserInstance} from '../../helpers/auth';

/**
 *
 * @param {Object} ctx
 * @returns {Promise<Object>}
 * @throws {Error}
 */
export async function get(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const notifications = await getNotifications(shopID);
    return (ctx.body = {
      data: notifications,
      success: true
    });
  } catch (e) {
    ctx.status = 404;
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}
