import {
  getSettings,
  updateSettings
} from '../../repositories/settingsRepository';
import {getCurrentUserInstance} from '../../helpers/auth';

const delay = ms => new Promise(res => setTimeout(res, ms));

/**
 *
 * @param {Object} ctx
 * @returns {Promise<Object>}
 * @throws {Error}
 */
export async function get(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const settings = await getSettings(shopID);

    return (ctx.body = {
      data: settings,
      success: true
    });
  } catch (e) {
    ctx.status = 404;
    return (ctx.body = {
      data: {},
      success: false
    });
  }
}

/**
 *
 * @param {Object} ctx
 * @returns {Promise<Object>}
 * @throws {Error}
 */
export async function update(ctx) {
  try {
    const {shopID} = getCurrentUserInstance(ctx);
    const data = ctx.req.body;
    await updateSettings(shopID, data);
    await delay(3000);
    return (ctx.body = {
      success: true
    });
  } catch (e) {
    return (ctx.body = {
      success: false
    });
  }
}
