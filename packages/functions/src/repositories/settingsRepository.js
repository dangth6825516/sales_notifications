import {Firestore} from '@google-cloud/firestore';
import defaultSettings from '../consts/defaultSettings';

const firestore = new Firestore();
const settingsRef = firestore.collection('settings');

/**
 *
 * @param {string} shopId
 * @returns {Promise<QuerySnapshot>}
 */
async function getSettingsRefById(shopId) {
  return await settingsRef
    .where('shopId', '=', shopId)
    .limit(1)
    .get();
}

/**
 *
 * @param {string} shopId
 * @returns {Promise<Object|null>}
 */
export async function getSettings(shopId) {
  const settingsDocs = await getSettingsRefById(shopId);
  if (settingsDocs.empty) return null;
  const settingsDoc = settingsDocs.docs[0];
  return settingsDoc.data();
}

/**
 *
 * @param {string} shopId
 * @param {Object} data
 * @returns {Promise<void>}
 */
export async function updateSettings(shopId, data) {
  const settingsDocs = await getSettingsRefById(shopId);
  const docRef = settingsDocs.docs[0].ref;
  return await docRef.set(data, {merge: false});
}

/**
 *
 * @param {string} shopId
 * @returns {Promise<void>}
 */
export async function createDefaultSettings(shopId) {
  const settingsDocs = await getSettingsRefById(shopId);
  if (!settingsDocs.empty) return;
  const settings = {...defaultSettings, shopId};
  return await settingsRef.add(settings);
}
