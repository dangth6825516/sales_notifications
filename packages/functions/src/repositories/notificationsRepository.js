import {Firestore} from '@google-cloud/firestore';
import formatDateFields from '../helpers/formatDateFields';
const firestore = new Firestore();

const notificationsRef = firestore.collection('notifications');

/**
 *
 * @param {string} shopId
 * @returns {Promise<Array<Object>|null>}
 */
export async function getNotifications(shopId) {
  const notificationsDocs = await notificationsRef
    .where('shopId', '=', shopId)
    .get();

  if (notificationsDocs.empty) {
    return null;
  }
  return notificationsDocs.docs.map(notification => {
    const data = notification.data();
    return {...formatDateFields(data), id: notification.id};
  });
}

/**
 *
 * @param {Array<Object>} data
 * @returns {Promise<void>}
 */
export async function createNotifications(data) {
  const batch = firestore.batch();
  data.forEach(doc => {
    const docRef = notificationsRef.doc();
    batch.set(docRef, doc);
  });

  return await batch.commit();
}
