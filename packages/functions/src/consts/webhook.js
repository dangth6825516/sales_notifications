const webhook = {
  topic: 'orders/create',
  address: 'https://250a-14-232-137-85.ngrok-free.app/webhook/order/new',
  format: 'json'
};

export default webhook;
