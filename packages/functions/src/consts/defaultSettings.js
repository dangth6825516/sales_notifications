const defaultSettings = {
  allowShow: 'all',
  displayDuration: 3,
  excludedUrls: '',
  firstDelay: 0,
  hideTimeAgo: false,
  includedUrls: '',
  maxPopsDisplay: 3,
  popsInterval: 3,
  position: 'bottom-left',
  truncateProductName: false
};

export default defaultSettings;
