import {insertAfter} from '../helpers/insertHelpers';
import {render} from 'preact';
import React from 'preact/compat';
import NotificationPopup from '../components/NotificationPopup/NotificationPopup';
import {ALL, SPECIFIC} from '../consts/display';

export default class DisplayManager {
  constructor() {
    this.notifications = [];
    this.settings = {};
  }

  async initialize({notifications, settings}) {
    this.notifications = notifications;
    this.settings = settings;

    if (!this.checkCanDisplay(settings)) return;

    this.insertContainer();

    const firstDelay = settings.firstDelay * 1000;
    const displayDuration = settings.displayDuration * 1000;
    const popsInterval = settings.popsInterval * 1000;
    const notificationData = notifications.slice(0, settings.maxPopsDisplay);

    await this.delay(firstDelay);

    for (const notification of notificationData) {
      const popup = {
        ...notification,
        ...settings
      };
      await this.display(popup);
      await this.delay(displayDuration);
      await this.fadeOut();
      await this.delay(popsInterval);
    }
  }

  checkCanDisplay(settings) {
    const isIncluded = this.checkUrl(settings.includedUrls);
    const isExcluded = this.checkUrl(settings.excludedUrls);

    if (settings.allowShow === ALL) return !isExcluded;
    else if (settings.allowShow === SPECIFIC) return isIncluded && !isExcluded;
    else return false;
  }

  checkUrl(urlString) {
    const currentUrl = window.location.href;
    const urls = urlString.split('\n');
    return urls.some(url => url && currentUrl.startsWith(url));
  }

  delay = ms => new Promise(res => setTimeout(res, ms));

  fadeOut() {
    const container = document.querySelector('#Avada-SalePop');
    container.innerHTML = '';
  }

  display(popup) {
    const container = document.querySelector('#Avada-SalePop');
    render(<NotificationPopup {...popup} />, container);
  }

  insertContainer() {
    const popupEl = document.createElement('div');
    popupEl.id = `Avada-SalePop`;
    popupEl.classList.add('Avada-SalePop__OuterWrapper');
    const targetEl = document.querySelector('body').firstChild;
    if (targetEl) {
      insertAfter(popupEl, targetEl);
    }

    return popupEl;
  }
}
